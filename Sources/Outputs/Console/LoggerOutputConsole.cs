﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Iodynis.Libraries.Logging
{
    /// <summary>
    /// Logger output to the console.
    /// </summary>
    public class LoggerOutputConsole : ALoggerOutput
    {
        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        private static extern bool AttachConsole(int dwProcessId);
        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        private static extern bool AllocConsole();

        private const int ATTACH_PARENT_PROCESS = -1;

        private static volatile StreamWriter ConsoleStreamWriter;
        private readonly object Lock = new object();

        ///// <summary>
        ///// Check if a black console window is presented.
        ///// </summary>
        ///// <returns>True if there is a console. False otherwise.</returns>
        //public static bool IsConsolePresented()
        //{
        //    // HACK: As the console existence cannot be checked in a civilized way, play a little try-catch game
        //    try
        //    {
        //        return Console.WindowHeight >= 0;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}

        /// <summary>
        /// Visual formatting of the file.
        /// </summary>
        public readonly LoggerOutputConsoleFormat Format = new LoggerOutputConsoleFormat();

        /// <summary>
        /// Instantiate a logger to the console.
        /// </summary>
        /// <param name="name">The name of the logger.</param>
        public LoggerOutputConsole(string name = "Console")
            : base(name)
        {
            //var stdout = Console.OpenStandardOutput();
            //field_stdOutWriter = new StreamWriter(stdout, Encoding.UTF8);
            //field_stdOutWriter.AutoFlush = true;
            //AttachConsole(ATTACH_PARENT_PROCESS);
            //field_stdOutWriter.WriteLine();
            //Console.WriteLine();

            // --------------------------------------------------------- //
            // Attach to the parent console window if there is one.
            // This needs to happen before AttachConsole.
            // If the output is not redirected we still get a valid stream but it doesn't appear to write anywhere
            // I guess it probably does write somewhere, but nowhere I can find out about

            //field_stdOut = Console.Out;
            //field_stdOut.WriteLine("WTF");
            //TextWriter w = TextWriter.

            //if (IsConsolePresented())
            //{
            //    Console.WriteLine("YES");
            //}
            //else
            //{
            //    Console.WriteLine("NO");
            //}

            var stdout = Console.OpenStandardOutput();
            ConsoleStreamWriter = new StreamWriter(stdout, Encoding.ASCII /* Console is not friendly with UTF8 resulting in ∩╗┐ as first symbols in the console (Unicode Byte Order Mask -- bytes EF BB BF) */)
            {
                AutoFlush = true
            };
            try
            {
                if (!AttachConsole(ATTACH_PARENT_PROCESS))
                {
                    //AllocConsole();
                }
            }
            catch { }
            ConsoleStreamWriter = null;
        }
        /// <summary>
        /// Destructor.
        /// </summary>
        ~LoggerOutputConsole()
        {
            try
            {
                lock (Lock)
				{
					ConsoleStreamWriter?.Flush();
					ConsoleStreamWriter?.Close();
                }

	            ConsoleStreamWriter = null;
            }
            catch { }
        }
        /// <summary>
        /// Log text to console.
        /// </summary>
        /// <param name="channel"><inheritdoc cref="ALoggerOutput.Log(string, string, LoggerType, DateTime, int)" path="/param[1]"/></param>
        /// <param name="text"><inheritdoc cref="ALoggerOutput.Log(string, string, LoggerType, DateTime, int)" path="/param[2]"/></param>
        /// <param name="type"><inheritdoc cref="ALoggerOutput.Log(string, string, LoggerType, DateTime, int)" path="/param[3]"/></param>
        /// <param name="time"><inheritdoc cref="ALoggerOutput.Log(string, string, LoggerType, DateTime, int)" path="/param[4]"/></param>
        /// <param name="indentation"><inheritdoc cref="ALoggerOutput.Log(string, string, LoggerType, DateTime, int)" path="/param[5]"/></param>
        public override void Log(string channel, string text, LoggerType type, DateTime time, int indentation)
        {
            if (!IsEnabled)
            {
                return;
            }

            // Type
            string typeString = "";
            if (Format.LogType)
            {
                switch (type)
                {
                        // Raw data
                    case LoggerType.NONE:
                        typeString = Format.TypeEmpty;
                        break;
                    // Trace info
                    case LoggerType.TRACE:
                        typeString = Format.TypeTrace;
                        break;
                    // Debug info
                    case LoggerType.DEBUG:
                        typeString = Format.TypeDebug;
                        break;
                    // Info about current process (main variables & results of main functions)
                    case LoggerType.INFORMATION:
                        typeString = Format.TypeInformation;
                        break;
                    // Success
                    case LoggerType.SUCCESS:
                        typeString = Format.TypeSuccess;
                        break;
                    // A immensely  huge success
                    case LoggerType.WIN:
                        typeString = Format.TypeWin;
                        break;
                    // Notice
                    case LoggerType.NOTICE:
                        typeString = Format.TypeNotice;
                        break;
                    // Cautions
                    case LoggerType.CAUTION:
                        typeString = Format.TypeCaution;
                        break;
                    // Warnings
                    case LoggerType.WARNING:
                        typeString = Format.TypeWarning;
                        break;
                    // Handled errors
                    case LoggerType.ERROR:
                        typeString = Format.TypeError;
                        break;
                    // Unrecoverable errors
                    case LoggerType.FATALITY:
                        typeString = Format.TypeFatality;
                        break;
                    // An unsupported log level
                    default:
                        throw new LoggerLevelException($"Logger level {type} is not supported.");
                }
            }

            // Timestamp
            string timeStampString = "";
            if (Format.LogTimeStamp)
            {
                timeStampString = time.ToString(Format.TimeStampFormat);
            }

            // Color
            ConsoleColor consoleColorForeground;
            ConsoleColor consoleColorBackground;
            switch (type)
            {
                case LoggerType.NONE:
                    consoleColorForeground = ConsoleColor.White;
                    consoleColorBackground = ConsoleColor.Black;
                    break;
                case LoggerType.TRACE:
                    consoleColorForeground = ConsoleColor.DarkGray;
                    consoleColorBackground = ConsoleColor.Black;
                    break;
                case LoggerType.DEBUG:
                    consoleColorForeground = ConsoleColor.DarkGray;
                    consoleColorBackground = ConsoleColor.Black;
                    break;
                case LoggerType.INFORMATION:
                    consoleColorForeground = ConsoleColor.Gray;
                    consoleColorBackground = ConsoleColor.Black;
                    break;
                case LoggerType.SUCCESS:
                    consoleColorForeground = ConsoleColor.Green;
                    consoleColorBackground = ConsoleColor.Black;
                    break;
                case LoggerType.WIN:
                    consoleColorForeground = ConsoleColor.Black;
                    consoleColorBackground = ConsoleColor.Green;
                    break;
                case LoggerType.NOTICE:
                    consoleColorForeground = ConsoleColor.DarkYellow;
                    consoleColorBackground = ConsoleColor.Black;
                    break;
                case LoggerType.CAUTION:
                    consoleColorForeground = ConsoleColor.DarkYellow;
                    consoleColorBackground = ConsoleColor.Black;
                    break;
                case LoggerType.WARNING:
                    consoleColorForeground = ConsoleColor.Yellow;
                    consoleColorBackground = ConsoleColor.Black;
                    break;
                case LoggerType.ERROR:
                    consoleColorForeground = ConsoleColor.Red;
                    consoleColorBackground = ConsoleColor.Black;
                    break;
                case LoggerType.FATALITY:
                    consoleColorForeground = ConsoleColor.Black;
                    consoleColorBackground = ConsoleColor.Red;
                    break;
                default:
                    throw new LoggerLevelException($"Logger level {type} is not supported.");
            }

            // Channel name
            string channelString = String.IsNullOrEmpty(channel) ? "" : $"{channel} ";

            // Indentation
            string indentationString = GetIndentation(indentation);

            // Split into lines
            string[] textStrings = text == null || text == "" ? new string[] { "" } : text.Split(new char[] { '\n' }, StringSplitOptions.None);
            for (int index = 0; index < textStrings.Length; index++)
            {
                textStrings[index] = textStrings[index].TrimEnd('\r');
            }

            // Set colors
            Console.ForegroundColor = consoleColorForeground;
            Console.BackgroundColor = consoleColorBackground;

            // Log every line
            for (int index = 0; index < textStrings.Length; index++)
            {
                // Log a new line
                ConsoleStreamWriter?.Write(Environment.NewLine);
                Console.Write(Environment.NewLine);

                string line = timeStampString + typeString + channelString + indentationString + textStrings[index];

                // Log the text
                ConsoleStreamWriter?.Write(line);
                Console.Write(line);
            }

            // Reset colors
            Console.ResetColor();
        }
        /// <summary>
        /// Append text to the console.
        /// </summary>
        /// <param name="text">Text to append.</param>
        /// <param name="delimiter">Delimiter to append before the text.</param>
        public override void Append(string text, string delimiter = null)
        {
            if (!IsEnabled)
            {
                return;
            }

            ConsoleStreamWriter?.Write(delimiter ?? Format.AppendDelimiter);
            ConsoleStreamWriter?.Write(text);
            Console.Write(delimiter ?? Format.AppendDelimiter);
            Console.Write(text);
        }

        private static List<string> Indentations = new List<string>();
        private string GetIndentation(int indentation)
        {
            if (indentation < 0)
            {
                return "";
            }
            if (indentation >= Indentations.Count)
            {
                lock (Indentations)
                {
                    while (indentation >= Indentations.Count)
                    {
                        Indentations.Add(Indentations.Count == 0 ? "" : Indentations[Indentations.Count - 1] + Format.ScopeIndentation);
                    }
                }
            }
            return Indentations[indentation];
        }
    }
}
