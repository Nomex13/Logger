﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;

namespace Iodynis.Libraries.Logging
{
    /// <summary>
    /// Logger output to a file.
    /// </summary>
    public class LoggerOutputFile : ALoggerOutput
    {
        private volatile Thread CallerThread = Thread.CurrentThread;

        private volatile FileStream    FileStream;
        private volatile StreamWriter  FileStreamWriter;
        private volatile StringBuilder FileStreamBuffer = new StringBuilder(1024);
        private volatile int           FileStreamBufferOverflow = 0;
        private volatile Thread        LoopThread;
        private readonly object Lock = new object();

        private DateTime TimeLast = DateTime.MinValue;
        private LoggerType LogTypeLast = LoggerType.NONE;
        private string ChannelLast = null;
        private int ThreadLast = -1;

        /// <summary>
        /// Store logs in memory if they can't be written to the file (path not set, file locked, etc).
        /// Stored logs will be written to the file as soon as it becomes possible.
        /// Maximum buffer size is detemined by the <see cref="BufferSize"/>.
        /// </summary>
        public bool UseBuffer { get; set; } = true;
        /// <summary>
        /// Maximum buffer string length to store the logs in memory.
        /// Actual size in memory will be 2-4 times greater because of the UTF16 encoding.
        /// Set to zero or negative value to disable this limit.
        /// </summary>
        public int BufferSize { get; set; } = 1024 * 1024; // =2-4 MB (because of UTF16)
        /// <summary>
        /// Maximum count of logs in the plain text file.
        /// </summary>
        public int MaximumLogsInTextFile { get; set; } = 3;
        /// <summary>
        /// Maximum count of logs in the zip archive.
        /// The oldest logs in the zip archive will be deleted to preserve its size below this limit.
        /// </summary>
        public int MaximumLogsInZipFile { get; set; } = 100;
        /// <summary>
        /// Maximum size of the zip archive with logs (in bytes).
        /// The oldest logs in the zip archive will be deleted to preserve its size below this limit.
        /// </summary>
        public int MaximumSizeOfZipFile { get; set; } = 10 * 1024 * 1024;
        /// <summary>
        /// Should oldest logs be moved to a zip archive when their count in the plain text file exceeds the maximum count <see cref="MaximumLogsInTextFile"/>.
        /// </summary>
        public bool ZipOldLogs { get; set; } = false;
        /// <summary>
        /// Log text file extension.
        /// </summary>
        private string FileExtension { get; set; } = "txt";
        /// <summary>
        /// Log text and zip files name without extension.
        /// </summary>
        private string FileNameWithoutExtension { get; set; } = "log";
        /// <summary>
        /// Log text file name with extension.
        /// </summary>
        private string FileName{ get; set; } = "log.txt";
        /// <summary>
        /// Log zip file name with extension.
        /// </summary>
        private string FileNameZipped{ get; set; } = "log.zip";
        /// <summary>
        /// Log files directory (used both for the plain text one and the zip archive one).
        /// </summary>
        private string FileDirectory { get; set; } = "./";
        /// <summary>
        /// Visual formatting of the file.
        /// </summary>
        public readonly LoggerOutputFileFormat Format = new LoggerOutputFileFormat();

        private string _FilePath = "./log.txt";
        /// <summary>
        /// Log text file full path.
        /// </summary>
        public string FilePath
        {
            get
            {
                return _FilePath;
            }
            set
            {
                if (String.Equals(_FilePath, value))
                {
                    return;
                }

                // Close the file
                if (FileStream != null)
                {
                    FileClose();
                }

                if (value == null)
                {
                    _FilePath = null;
                    FileDirectory = null;
                    FileName = null;
                    FileNameWithoutExtension = null;
                    FileExtension = null;
                    FileNameZipped = null;
                }
                else
                {
                    // Change path and extract everything from it
                    _FilePath = Path.GetFullPath(value).Replace('\\', '/');
                    FileDirectory = Path.GetDirectoryName(_FilePath);
                    if (FileDirectory == null)
                    {
                        FileDirectory = "";
                    }
                    else if (!FileDirectory.EndsWith("/"))
                    {
                        FileDirectory += "/";
                    }
                    FileName = Path.GetFileName(_FilePath);
                    FileNameWithoutExtension = Path.GetFileNameWithoutExtension(_FilePath);
                    // Cut the leading dot(s) from the extension
                    string extension = Path.GetExtension(_FilePath);
                    while (extension.Length > 0 && extension[0] == '.')
                    {
                        extension = extension.Substring(1);
                    }
                    FileExtension = extension;
                    FileNameZipped = FileNameWithoutExtension + ".zip";
                }
            }
        }

        /// <summary>
        /// Instantiate a logger to write to the specified file.
        /// </summary>
        /// <param name="name">The name of the logger.</param>
        /// <param name="path">The path to the file to write logs to.</param>
        public LoggerOutputFile(string name, string path)
            : base(name)
        {
            FilePath = path;
        }
        /// <summary>
        /// Instantiate a logger to write to the file of the provided name and the log extension in the current directory.
        /// </summary>
        /// <param name="name">The name of the logger.</param>
        public LoggerOutputFile(string name = "File")
            : base(name)
        {
            FilePath = Path.Combine(Directory.GetCurrentDirectory(), $"{FileName}.log");
        }
        /// <summary>
        /// Destructor.
        /// </summary>
        ~LoggerOutputFile()
        {
            try
            {
                LoopStop();

                lock (Lock)
				{
					LoopThread?.Abort();
                }

	            LoopThread = null;
            }
            catch { }

            try
            {
                // Read buffer
                StringBuilder fileStreamBuffer = FileStreamBuffer;
                FileStream fileStream = FileStream;
                if (fileStreamBuffer != null && fileStream != null)
                {
                    string buffer = fileStreamBuffer.ToString();
                    if (buffer != "")
                    {
                        FileStreamWriter.Write(buffer);
                    }
                    FileStream.Flush();
                    FileStream.Close();
                }
            }
            catch { }
        }
        /// <summary>
        /// File logger initialization logic.
        /// </summary>
        protected override void InitializeInternal()
        {
            LoopStart();
        }
        /// <summary>
        /// File logger deinitialization logic.
        /// </summary>
        protected override void DeinitializeInternal()
        {
            LoopStop();
        }
        /// <summary>
        /// Log text to the file.
        /// </summary>
        /// <param name="channel"><inheritdoc cref="ALoggerOutput.Log(string, string, LoggerType, DateTime, int)" path="/param[1]"/></param>
        /// <param name="text"><inheritdoc cref="ALoggerOutput.Log(string, string, LoggerType, DateTime, int)" path="/param[2]"/></param>
        /// <param name="type"><inheritdoc cref="ALoggerOutput.Log(string, string, LoggerType, DateTime, int)" path="/param[3]"/></param>
        /// <param name="time"><inheritdoc cref="ALoggerOutput.Log(string, string, LoggerType, DateTime, int)" path="/param[4]"/></param>
        /// <param name="indentation"><inheritdoc cref="ALoggerOutput.Log(string, string, LoggerType, DateTime, int)" path="/param[5]"/></param>
        public override void Log(string channel, string text, LoggerType type, DateTime time, int indentation)
        {
            if (!IsEnabled)
            {
                return;
            }
            if (ThreadsAllowed.Count > 0 && !ThreadsAllowed.Contains(Thread.CurrentThread.ManagedThreadId))
            {
                return;
            }
            if (ThreadsForbidden.Count > 0 && ThreadsForbidden.Contains(Thread.CurrentThread.ManagedThreadId))
            {
                return;
            }
            if (type != LoggerType.NONE && SeverityThreshold != LoggerSeverity.NONE && type.ToSeverity() < SeverityThreshold)
            {
                return;
            }
            if (FileStream == null && !UseBuffer)
            {
                return;
            }

            // Type
            string typeString = "";
            if (Format.LogType)
            {
                if (type == LoggerType.NONE)
                {
                    typeString = Format.TypeEmpty;
                }
                else if (Format.OmitSameThread && type == LogTypeLast)
                {
                    typeString = Format.TypeEmpty;
                }
                else
                {
                    switch (type)
                    {
                        // Raw data
                        case LoggerType.NONE:
                            typeString = Format.TypeEmpty;
                            break;
                        // Trace info
                        case LoggerType.TRACE:
                            typeString = Format.TypeTrace;
                            break;
                        // Debug info
                        case LoggerType.DEBUG:
                            typeString = Format.TypeDebug;
                            break;
                        // Info about current process (main variables & results of main functions)
                        case LoggerType.INFORMATION:
                            typeString = Format.TypeInformation;
                            break;
                        // Success
                        case LoggerType.SUCCESS:
                            typeString = Format.TypeSuccess;
                            break;
                        // A immensely  huge success
                        case LoggerType.WIN:
                            typeString = Format.TypeWin;
                            break;
                        // Notice
                        case LoggerType.NOTICE:
                            typeString = Format.TypeNotice;
                            break;
                        // Cautions
                        case LoggerType.CAUTION:
                            typeString = Format.TypeCaution;
                            break;
                        // Warnings
                        case LoggerType.WARNING:
                            typeString = Format.TypeWarning;
                            break;
                        // Handled errors
                        case LoggerType.ERROR:
                            typeString = Format.TypeError;
                            break;
                        // Unrecoverable errors
                        case LoggerType.FATALITY:
                            typeString = Format.TypeFatality;
                            break;
                        // An unsupported log level
                        default:
                            throw new Exception($"Logger type {type} is not supported.");
                    }
                    LogTypeLast = type;
                }
            }

            // Timestamp
            string timeStampString = "";
            if (Format.LogTimeStamp)
            {
                if (Format.OmitSameTimestamp && TimeLast == time)
                {
                    timeStampString = Format.TimeStampDefault;
                }
                else
                {
                    timeStampString = time.ToString(Format.TimeStampFormat);
                    TimeLast = time;
                }
            }

            // Thread ID
            string threadString = "";
            if (Format.LogThread)
            {
                int threadId = Thread.CurrentThread.ManagedThreadId;
                if (Format.OmitSameThread && ThreadLast == threadId)
                {
                    threadString = Format.ThreadDefault;
                }
                else if (Format.OmitMainThread && threadId == 1)
                {
                    threadString = Format.ThreadDefault;
                    ThreadLast = threadId;
                }
                else
                {
                    threadString = String.Format("{0,8:X}", threadId);

                    // Cut the thread ID if it is too long and allowed to actually cut it
                    if (Format.ThreadLengthForce && threadString.Length > Format.ThreadLength)
                    {
                        threadString = threadString.Substring(0, Format.ThreadLength);
                    }
                    ThreadLast = threadId;
                }
            }

            // Channel name
            string channelString = Format.ChannelDefault;
            if (Format.LogChannel && channel != null)
            {
                if (Format.OmitSameChannel && ChannelLast == channel)
                {
                    channelString = Format.ChannelDefault;
                }
                // Cut the channel name if it is too long and allowed to actually cut it
                else if (Format.ChannelLengthForce && channel.Length > Format.ChannelLength)
                {
                    channelString = channel.Substring(0, Format.ChannelLength);
                    ChannelLast = channel;
                }
                // Pad the channel name if it is too short
                else if (channel.Length < Format.ChannelLength)
                {
                    channelString = channel + (Format.ChannelLength - channel.Length).Times(" ");
                    ChannelLast = channel;
                }
                // Use the channel name as is
                else
                {
                    channelString = channel;
                    ChannelLast = channel;
                }
            }

            // Indentation
            string indentationString = GetIndentation(indentation);

            // Split into lines
            string[] textStrings = text == null || text == "" ? new string[] { "" } : text.Split(new char[] { '\n' }, StringSplitOptions.None);
            for (int index = 0; index < textStrings.Length; index++)
            {
                textStrings[index] = textStrings[index].TrimEnd('\r');
            }

            // Add to the buffer
            lock (FileStreamBuffer)
            {
                for (int index = 0; index < textStrings.Length; index++)
                {
                    FileStreamBuffer.Append(Format.NewLine);

                    if (Format.LogThread)
                    {
                        FileStreamBuffer.Append(Format.OmitSameThread && index > 0 ? Format.ThreadDefault : threadString);
                        FileStreamBuffer.Append(" ");
                    }
                    if (Format.LogChannel)
                    {
                        FileStreamBuffer.Append(Format.OmitSameChannel && index > 0 ? Format.ChannelDefault : channelString);
                        FileStreamBuffer.Append(" ");
                    }
                    if (Format.LogTimeStamp)
                    {
                        FileStreamBuffer.Append(Format.OmitSameTimestamp && index > 0 ? Format.TimeStampDefault : timeStampString);
                        FileStreamBuffer.Append(" ");
                    }

                    if (Format.LogTimeStamp || Format.LogThread || Format.LogChannel || Format.LogType)
                    {
                        FileStreamBuffer.Append("| ");
                    }

                    if (Format.LogType)
                    {
                        FileStreamBuffer.Append(Format.OmitSameType && index > 0 ? Format.TypeEmpty : typeString);
                        FileStreamBuffer.Append(" ");
                    }

                    FileStreamBuffer.Append(indentationString);
                    FileStreamBuffer.Append(textStrings[index]);
                }
                // Keep the buffer under the limits
                if (FileStreamBuffer.Length > BufferSize)
                {
                    int overflow = FileStreamBuffer.Length - BufferSize;
                    FileStreamBufferOverflow += overflow;
                    FileStreamBuffer.Remove(0, overflow);
                }
            }
        }
        /// <summary>
        /// Append text to the file.
        /// </summary>
        /// <param name="text">Text to append.</param>
        /// <param name="delimiter">Delimiter to append before the text.</param>
        public override void Append(string text, string delimiter = null)
        {
            if (!IsEnabled)
            {
                return;
            }

            if (FileStream == null && !UseBuffer)
            {
                return;
            }

            // Add to the buffer
            lock (FileStreamBuffer)
            {
                FileStreamBuffer.Append(delimiter ?? Format.AppendDelimiter);
                FileStreamBuffer.Append(text);
                // Keep the buffer under the limits
                if (FileStreamBuffer.Length > BufferSize)
                {
                    int overflow = FileStreamBuffer.Length - BufferSize;
                    FileStreamBufferOverflow += overflow;
                    FileStreamBuffer.Remove(0, overflow);
                }
            }
        }

        #region File

        private void FilePrepare()
        {
            FilePrepare(MaximumLogsInTextFile, ZipOldLogs);
        }
        private void FilePrepare(int maximumLogsInOneFile, bool zipOldLogs)
        {
            if (FileStream == null)
            {
                throw new Exception("File is not opened.");
            }
            // Check permissions are granted
            if (!FileStream.CanRead)
            {
                return;
            }
            if (!FileStream.CanWrite)
            {
                return;
            }
            if (!FileStream.CanSeek)
            {
                return;
            }
            // If maximum option is off -- do not touch anything
            if (maximumLogsInOneFile <= 0)
            {
                return;
            }
            // If only one log per file allowed and it should not be archived -- just overwrite the file
            if (maximumLogsInOneFile == 1 && !zipOldLogs)
            {
                // Clear the file
                FileStream.SetLength(0);
                FileStream.Seek(0, SeekOrigin.Begin);
                // Write the beginning
                FileStreamWriter.Write(Format.BeginningWithTimeStamp);
                return;
            }
            // Analyze and modify the log file
            try
            {
                // Check if empty
                if (FileStream.Length == 0)
                {
                    // Write the beginning
                    FileStream.Seek(0, SeekOrigin.Begin);
                    FileStreamWriter.Write(Format.BeginningWithTimeStamp);
                    FileStreamWriter.Flush();
                    return;
                }

                string timeStampCurrent = DateTime.Now.ToString(Format.TimeStampFormat);

                // Add a special ending to the last log if it interrupts unexpectedly
                {
                    bool endingFailed = false;
                    int endingBytesCount = Encoding.UTF8.GetBytes(Format.EndingWithTimestamp).Length;
                    byte[] endingBytes = new byte[endingBytesCount];
                    if (FileStream.Length != 0)
                    {
                        if (FileStream.Length < endingBytesCount)
                        {
                            endingFailed = true;
                        }
                        else
                        {
                            FileStream.Seek(FileStream.Length - endingBytesCount, SeekOrigin.Begin);
                            FileStream.Read(endingBytes, 0, endingBytesCount);
                            string ending = Encoding.UTF8.GetString(endingBytes);

                            string endingActualWithoutTimeStamp = ending;
                            string endingTemplateWithoutTimeStamp = Format.Ending;
                            int timestampIndex;
                            while ((timestampIndex = endingTemplateWithoutTimeStamp.IndexOf(Format.TimeStampKeyword)) >= 0)
                            {
                                endingTemplateWithoutTimeStamp = endingTemplateWithoutTimeStamp.Substring(0, timestampIndex) + endingTemplateWithoutTimeStamp.Substring(timestampIndex + Format.TimeStampKeyword.Length);
                                endingActualWithoutTimeStamp = endingActualWithoutTimeStamp.Substring(0, timestampIndex) + endingActualWithoutTimeStamp.Substring(timestampIndex + timeStampCurrent.Length);
                            }
                            endingFailed = endingActualWithoutTimeStamp != endingTemplateWithoutTimeStamp;
                        }
                    }
                    if (endingFailed)
                    {
                        FileStream.Seek(-1, SeekOrigin.End);

                        if (FileStream.ReadByte() != '\n')
                        {
                            FileStreamWriter.Write(Environment.NewLine);
                        }

                        FileStreamWriter.Write(Format.AbruptEndingWithTimestamp);
                        FileStreamWriter.Flush();
                    }
                }

                // Find all delimiters
                long length = FileStream.Length;
                List<long> startIndexes = new List<long>() { 0 };
                List<long> endIndexes = new List<long>() { };
                long maybeEndIndex = -1;
                bool maybeEnd = false;
                bool skip = false;
                int newLinesCount = 0;
                FileStream.Seek(0, SeekOrigin.Begin);
                for (long index = 0; index < length; index++)
                {
                    int @byte = FileStream.ReadByte();
                    if (@byte < 0)
                    {
                        break;
                    }
                    // Multibyte symbol ends
                    if (skip)
                    {
                        skip = false;
                        continue;
                    }
                    // New line
                    if (@byte == '\n')
                    {
                        newLinesCount++;
                        if (!maybeEnd)
                        {
                            maybeEndIndex = index - 1;
                            maybeEnd = true;
                        }
                    }
                    else if (@byte == '\r')
                    {
                        if (!maybeEnd)
                        {
                            maybeEndIndex = index - 1;
                            maybeEnd = true;
                        }
                    }
                    // Other symbol
                    else
                    {
                        // Multibyte symbol starts or continues
                        if (@byte > 127)
                        {
                            skip = true;
                        }
                        // Just started a new line
                        if (newLinesCount == 1)
                        {
                            newLinesCount = 0;
                        }
                        // Just started a new log part
                        else if (newLinesCount > 1)
                        {
                            newLinesCount = 0;
                            startIndexes.Add(index);
                            if (maybeEndIndex > 0)
                            {
                                endIndexes.Add(maybeEndIndex);
                            }
                        }
                        maybeEnd = false;
                    }
                }
                endIndexes.Add(newLinesCount > 1 ? maybeEndIndex : FileStream.Length - 1);

                int oldLogsCountToKeep = maximumLogsInOneFile <= 0 ? int.MaxValue /* Keep everything */ : maximumLogsInOneFile - 1;
                long firstIndexToKeep =
                    //maximumLogsInOneFile <= 0 ? 0 : // Keep everything if maximum logs in one file option is not used
                    startIndexes.Count <= oldLogsCountToKeep ? 0 : // Keep everything if there are less logs in the file than the maximum allowed
                    oldLogsCountToKeep == 0 ? FileStream.Length : // Clear everything if no logs should be kept
                    startIndexes[startIndexes.Count - oldLogsCountToKeep]; // Clear logs partially

                // Archive old logs
                if (zipOldLogs)
                {
                    for (int startIndexesIndex = 0; startIndexesIndex < startIndexes.Count && startIndexes[startIndexesIndex] < firstIndexToKeep; startIndexesIndex++)
                    {
                        long startIndex = startIndexes[startIndexesIndex];
                        long endIndex   = endIndexes[startIndexesIndex];

                        // Read a small piece
                        int beginningLengthInBytes = Encoding.UTF8.GetBytes(Format.BeginningWithTimeStamp).Length;
                        int newLineLengthInBytes = Encoding.UTF8.GetBytes(Format.NewLine).Length;
                        int timeStampLengthInBytes = Encoding.UTF8.GetBytes(timeStampCurrent).Length;
                        byte[] buffer = new byte[timeStampLengthInBytes];
                        FileStream.Seek(startIndex + beginningLengthInBytes + newLineLengthInBytes, SeekOrigin.Begin);
                        FileStream.Read(buffer, 0, buffer.Length);

                        // Cut out the supposed timestamp and check if it really is one
                        string timeStampFound = Encoding.UTF8.GetString(buffer);
                        if (DateTime.TryParseExact(timeStampFound, Format.TimeStampFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dateTime))
                        {
                            string zipFilePath = Path.Combine(FileDirectory, FileNameZipped);

                            using (FileStream zipFileStream = new FileStream(zipFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None, 4096, FileOptions.None))
                            using (ZipArchive archive = new ZipArchive(zipFileStream, ZipArchiveMode.Update, false))
                            {
                                // Remove excessive logs (by files count)
                                if (MaximumLogsInZipFile > 0)
                                {
                                    int entriesToDeleteCount = archive.Entries.Count + 1 /* the new file */ - MaximumLogsInZipFile;
                                    if (entriesToDeleteCount > 0)
                                    {
                                        var entriesToDelete = archive.Entries.OrderBy(_entry => _entry.Name).Take(entriesToDeleteCount).ToList();
                                        foreach (ZipArchiveEntry entryToDelete in entriesToDelete)
                                        {
                                            entryToDelete.Delete();
                                        }
                                    }
                                }

                                // Create the new entry
                                try
                                {
                                    ZipArchiveEntry entry = archive.CreateEntry($"{dateTime:yyyyMMdd'.'HHmmss}.{FileExtension}");
                                    using (Stream entryStream = entry.Open())
                                    {
                                        byte[] bufferTxtToZip = new byte[4096];
                                        // Setup streams positions
                                        FileStream.Seek(startIndex, SeekOrigin.Begin);
                                        entryStream.Seek(0, SeekOrigin.Begin);
                                        while (true)
                                        {
                                            // Get the total count left
                                            long countTotal = endIndex - FileStream.Position + 1;
                                            if (countTotal <= 0)
                                            {
                                                break;
                                            }
                                            // Get the count to copy
                                            int countCurrent = countTotal > buffer.Length ? buffer.Length : (int)countTotal;
                                            // Copy bytes
                                            entryStream.Write(bufferTxtToZip, 0, FileStream.Read(bufferTxtToZip, 0, countCurrent));
                                        }
                                    }
                                }
                                catch (Exception exception)
                                {
                                    InvokeOnException(exception);
                                }
                            }

                            // Remove excessive logs (by archive size)
                            long sizeOfZipFile = 0;
                            try
                            {
                                sizeOfZipFile = File.Exists(zipFilePath) ? new FileInfo(zipFilePath).Length : 0;
                            }
                            catch
                            {
                                ;
                            }
                            using (FileStream zipFileStream = new FileStream(zipFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None, 4096, FileOptions.None))
                            using (ZipArchive archive = new ZipArchive(zipFileStream, ZipArchiveMode.Update, false))
                            {
                                if (MaximumSizeOfZipFile > 0)
                                {
                                    IEnumerable<ZipArchiveEntry> entries = archive.Entries.OrderBy(_entry => _entry.Name);
                                    Stack<ZipArchiveEntry> entriesToDelete = new Stack<ZipArchiveEntry>();
                                    foreach (ZipArchiveEntry entryToDelete in entries)
                                    {
                                        // Got enough free space
                                        if (sizeOfZipFile <= MaximumSizeOfZipFile)
                                        {
                                            break;
                                        }
                                        // Record the free space gain
                                        sizeOfZipFile -= entryToDelete.CompressedLength;
                                        entriesToDelete.Push(entryToDelete);
                                    }
                                    while (entriesToDelete.Count > 0)
                                    {
                                        entriesToDelete.Pop().Delete();
                                    }
                                }
                            }
                        }
                    }
                }

                // Clear the whole file
                if (firstIndexToKeep >= FileStream.Length)
                {
                    // Clear the file
                    FileStream.SetLength(0);
                    FileStream.Seek(0, SeekOrigin.Begin);
                    // Write the beginning
                    FileStreamWriter.Write(Format.BeginningWithTimeStamp);
                    FileStreamWriter.Flush();
                }
                // Clear the file partially
                else
                {
                    // Keep the whole file
                    if (firstIndexToKeep <= 0)
                    {
                        ;
                    }
                    // Move the remaining content of the file to the beginning
                    else
                    {
                        // Move it by chunks
                        long indexRead  = firstIndexToKeep;
                        long indexWrite = 0;
                        byte[] buffer = new byte[1024 * 4096];
                        while (true)
                        {
                            // Read
                            FileStream.Seek(indexRead, SeekOrigin.Begin);
                            int count = FileStream.Read(buffer, 0, buffer.Length);
                            // Check
                            if (count <= 0)
                            {
                                break;
                            }
                            // Write
                            FileStream.Seek(indexWrite, SeekOrigin.Begin);
                            FileStream.Write(buffer, 0, count);
                            // Shift
                            indexRead  += count;
                            indexWrite += count;
                        }
                        // Update file length
                        FileStream.SetLength(FileStream.Length - firstIndexToKeep);
                    }

                    // Add new lines to divide new logs from the old ones
                    FileStream.Seek(0, SeekOrigin.End);
                    FileStreamWriter.Write(Format.DelimiterWithNewLines);
                    FileStreamWriter.Write(Format.BeginningWithTimeStamp);
                    FileStreamWriter.Flush();
                }

                    //fileStream.Flush();
                //}
                FileStream.Seek(0, SeekOrigin.End);
            }
            catch (Exception exception)
            {
                InvokeOnException(exception);
            }
        }
        private void FileOpen()
        {
            if (FileStream != null)
            {
                throw new Exception($"Logger {Name} is already opened.");
            }
            try
            {
                // Init writer
                FileStream = new FileStream(_FilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read, 4096, FileOptions.WriteThrough);
                FileStreamWriter = new StreamWriter(FileStream, new UTF8Encoding(false), 4096, true)
                {
                    AutoFlush = false,
                    NewLine = Format.NewLine,
                };
            }
            catch (Exception exception)
            {
                throw new Exception($"Logger {Name} file stream could not be opened.", exception);
            }
        }
        private void FileClose()
        {
            if (FileStream == null)
            {
                throw new Exception($"Logger {Name} is already closed.");
            }
            try
            {
                // Append the pretty ending
                FileStream.Seek(0, SeekOrigin.End);
                FileStreamWriter.Write(Format.NewLine + Format.EndingWithTimestamp);

                // Flush the stream
                FileStreamWriter.Flush();
                FileStream.Flush();

                // Close the stream
                FileStreamWriter.Dispose();
                FileStream.Dispose();

                FileStreamWriter = null;
                FileStream = null;
            }
            catch (Exception exception)
            {
                throw new Exception($"Logger {Name} file stream could not be closed.", exception);
            }
        }
        private void FileReopen()
        {
            if (FileStream != null)
            {
                FileClose();
            }

            FileOpen();
            FilePrepare();
        }

        #endregion

        #region Loop

        private void LoopStart()
        {
            // Start the log thread
            if (LoopThread == null)
            {
                LoopThread = new Thread(Loop);
                LoopThread.Name = $"{Name}LoggerLoopThread";
                LoopThread.Start();
            }
        }
        private void LoopStop()
        {
            // Stop the log thread
			lock (Lock)
			{
                LoopThread?.Abort();
            }
            LoopThread = null;
        }
        private void Loop()
        {
            try
            {
                // Loop
                while ((CallerThread.ThreadState & ThreadState.Stopped) == 0 /* Not stopped */) //&& Thread.CurrentThread.ThreadState != ThreadState.AbortRequested)
                {
                    // Sleep a bit
                    //HACK: Cannot put this at the end of the loop because otherwise the final 0-100ms may not be written to the log.
                    Thread.Sleep(100);

                    // Try to open the stream if the buffer is not empty and the file path is specified
                    if (FileStream == null && FileStreamBuffer.Length > 0 && FilePath != null)
                    {
                        try
                        {
                            FileOpen();
                            FilePrepare();
                        }
                        catch (Exception exception)
                        {
                            if (FileStream != null)
                            {
                                try
                                {
                                    FileStream.Close();
                                }
                                catch { }
                            }
                            throw new Exception($"Failed to open {FilePath} file to write to.", exception);
                        }
                    }

                    if (FileStreamBuffer.Length > 0)
                    {
                        lock (FileStreamBuffer)
                        {
                            try
                            {
                                // Read the buffer
                                string buffer = FileStreamBuffer.ToString();

                                // Prepare the file
                                FileStream.Seek(0, SeekOrigin.End);

                                // Write an overflow info
                                if (FileStreamBufferOverflow > 0)
                                {
                                    // Try to modify the buffer to start on a new line
                                    int newLineIndex = buffer.IndexOf('\n');
                                    if (newLineIndex > 0 && newLineIndex < buffer.Length - 2 /* At least one symbol after the new line */)
                                    {
                                        buffer = buffer.Substring(newLineIndex + 1);
                                        FileStreamBufferOverflow += newLineIndex + 1;
                                    }
                                    // At least ensure that the first character is valid
                                    else if (buffer.Length > 0)
                                    {
                                        // Check if the buffer was cut through a surrogate pair
                                        if (Char.IsLowSurrogate(buffer[0]))
                                        {
                                            // Skip the surrogate pair leftovers
                                            buffer = buffer.Substring(1);
                                            FileStreamBufferOverflow += 1;
                                        }
                                    }

                                    // Write the overflow info to the log file
                                    FileStreamWriter.Write(Format.NewLine);
                                    if (Format.LogTimeStamp)
                                    {
                                        FileStreamWriter.Write(DateTime.Now.ToString(Format.TimeStampFormat));
                                        FileStreamWriter.Write(" ");
                                    }
                                    FileStreamWriter.Write($" ... {FileStreamBufferOverflow} symbols were lost due to the logger buffer overflow ...");
                                    // Reset the overflow
                                    FileStreamBufferOverflow = 0;
                                }

                                // Write the buffer
                                FileStreamWriter.Write(buffer);
                                FileStreamWriter.Flush();
                                // Reset the buffer
                                FileStreamBuffer.Clear();
                            }
                            catch (Exception exception)
                            {
                                InvokeOnException(exception);
                            }
                        }
                    }
                }
            }
            catch (ThreadAbortException)
            {
                ;
            }
            catch (Exception exception)
            {
                throw new Exception("Logging loop encountered an exception.", exception);
            }
            finally
            {
                // Close the stream
                if (FileStream != null)
                {
                    FileClose();
                }
            }
        }

        #endregion

        private static List<string> Indentations = new List<string>();
        private string GetIndentation(int indentation)
        {
            if (indentation < 0)
            {
                return "";
            }
            if (indentation >= Indentations.Count)
            {
                lock (Indentations)
                {
                    while (indentation >= Indentations.Count)
                    {
                        Indentations.Add(Indentations.Count == 0 ? "" : Indentations[Indentations.Count - 1] + Format.ScopeIndentation);
                    }
                }
            }
            return Indentations[indentation];
        }
    }
}
