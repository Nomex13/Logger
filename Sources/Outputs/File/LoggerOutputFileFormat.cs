﻿using System;
using System.Runtime.Remoting.Channels;
using System.Text.RegularExpressions;
using System.Threading;

namespace Iodynis.Libraries.Logging
{
    /// <summary>
    /// File formatting rules for the <see cref="LoggerOutputFile"/>.
    /// </summary>
    public class LoggerOutputFileFormat : LoggerOutputTextFormat
    {
        /// <summary>
        /// The string to replace with the timestamp value.
        /// By default it is %TIMESTAMP%.
        /// </summary>
        private string _TimeStampKeyword = "%TIMESTAMP%";
        /// <summary>
        /// <inheritdoc cref="_TimeStampKeyword"/>
        /// </summary>
        public string TimeStampKeyword
        {
            get
            {
                return _TimeStampKeyword;
            }
            set
            {
                _TimeStampKeyword = value;
                _Beginning = null;
                _Delimiter = null;
                _Ending = null;
                _AbruptEnding = null;
            }
        }

        private string _Beginning;
        /// <summary>
        /// The first line(s) of the new log instance in the file.
        /// <see cref="TimeStampKeyword"/> will be replaced with a timestamp.
        /// </summary>
        public string Beginning
        {
            get
            {
                return _Beginning ?? (_Beginning = GetBeginning());
            }
            set
            {
                _Beginning = value;
            }
        }

        private string GetBeginning()
        {
            int count =
                (LogTimeStamp ? TimeStampFormat.Length + 1 : 0) +
                //(Logger.LogType ? TypeEmpty.Length + 1 : 0) +
                (LogChannel ? ChannelLength + 1 : 0) +
                (LogThread ? ThreadLength + 1 : 0);

            return count.Times("─") + "┐" + Environment.NewLine
                // TODO: make this of dynamic size based on thread/channel/timestamp lengths
                + (LogThread ? " THREAD " : "") + (LogThread ? "  CHANNEL " : "") + (LogTimeStamp ? "     DATE        TIME    " : "") + "  |" + Environment.NewLine
                + (count - TimeStampFormat.Length - 1).Times(" ") + TimeStampKeyword + " |";
        }

        /// <summary>
        /// The first line(s) of the new log instance in the file.
        /// <see cref="TimeStampKeyword"/> is replaced by a timestamp of the current moment.
        /// </summary>
        public string BeginningWithTimeStamp
        {
            get
            {
                return Beginning.Replace(_TimeStampKeyword, DateTime.Now.ToString(TimeStampFormat));
            }
        }
        private readonly Regex regexDoubleNewLine = new Regex("(\r?\n)(\r?\n)");

        private string _Delimiter =
                                  @"     (`/\" +
            Environment.NewLine + @"     `=\/\" +
            Environment.NewLine + @"      `=\/\" +
            Environment.NewLine + @"       `=\/" +
            Environment.NewLine + @"          \_" +
            Environment.NewLine + @"          ) (" +
            Environment.NewLine + @"         (___)";

        /// <summary>
        /// A delimiter between different log instances inside one file.
        /// Before and after the delimiter blank lines will be automatically added.
        /// Do not use sequential new lines, consider adding spaces or something if necessary
        /// (double new lines are used to determine log borders).
        /// </summary>
        public string Delimiter
        {
            get
            {
                return _Delimiter;
            }
            set
            {
                if (regexDoubleNewLine.IsMatch(value))
                {
                    throw new Exception("Sorry, you cannot use double new lines inside the log delimiter. Double new lines are used to determine log borders. Consider adding a space or something.");
                }
                _Delimiter = value.Trim(new char[] { '\r', '\n' });
                _DelimiterWithNewLines = Environment.NewLine + " " /* Space will prevent log splitting here as split is done between double new lines */ + Environment.NewLine +
                    _Delimiter + Environment.NewLine /* Log splitting will be done here, after the delimiter, between the sequential new lines */ + Environment.NewLine;
            }
        }
        private string _DelimiterWithNewLines =
            Environment.NewLine + @" " +
            Environment.NewLine + @"     (`/\" +
            Environment.NewLine + @"     `=\/\" +
            Environment.NewLine + @"      `=\/\" +
            Environment.NewLine + @"       `=\/" +
            Environment.NewLine + @"          \_" +
            Environment.NewLine + @"          ) (" +
            Environment.NewLine + @"         (___)" +
            Environment.NewLine +
            Environment.NewLine;
        /// <summary>
        /// A delimiter between different log instances inside one file with new lines added before and after it.
        /// </summary>
        public string DelimiterWithNewLines
        {
            get
            {
                return _DelimiterWithNewLines;
            }
        }
        private string _Ending;
        /// <summary>
        /// The last line(s) of the new log instance in the file.
        /// <see cref="TimeStampKeyword"/> will be replaced with a timestamp.
        /// </summary>
        public string Ending
        {
            get
            {
                return _Ending ?? (_Ending = GetEnding());
            }
            set
            {
                _Ending = value;
            }
        }

        private string GetEnding()
        {
            int count =
                (LogTimeStamp ? TimeStampFormat.Length + 1 : 0) +
                //(Logger.LogType ? TypeEmpty.Length + 1 : 0) +
                (LogChannel ? ChannelLength + 1 : 0) +
                (LogThread ? ThreadLength + 1 : 0);

            return
                TimeStampDefault + (count - TimeStampFormat.Length).Times(" ") + "|" + Environment.NewLine +
                count.Times("─") + "┘" + Environment.NewLine +
                (count - 6).Times("─") + "┘  |" + Environment.NewLine +
                (count - 3).Times("─") + "┘" + Environment.NewLine;
        }

        /// <summary>
        /// The last line(s) of the new log instance in the file.
        /// <see cref="TimeStampKeyword"/> is replaced by a timestamp of the current moment.
        /// </summary>
        internal string EndingWithTimestamp
        {
            get
            {
                return Ending.Replace(_TimeStampKeyword, DateTime.Now.ToString(TimeStampFormat));
            }
        }

        // TODO: Make it 9 symbols wider
        private string _AbruptEnding;

        /// <summary>
        /// The last line(s) of an old log instance in the file if logging was abruptly terminated.
        /// <see cref="TimeStampKeyword"/> will be replaced with a timestamp.
        /// </summary>
        public string AbruptEnding
        {
            get
            {
                return _AbruptEnding ?? (_AbruptEnding = GetAbruptEnding());
            }
            set
            {
                _AbruptEnding = value;
            }
        }

        private string GetAbruptEnding()
        {
            int count =
                (LogTimeStamp ? TimeStampFormat.Length + 1 : 0) +
                //(Logger.LogType ? TypeEmpty.Length + 1 : 0) +
                (LogChannel ? ChannelLength + 1 : 0) +
                (LogThread ? ThreadLength + 1 : 0);

            string line0 = TimeStampDefault + (count - TimeStampFormat.Length - 1).Times(" ") + @"/";
            string line1 = @"    ___              __            __          _    /";
            string line2 = @"  _/   \  __       _/  \  /\_     /  \      /\/ \  /";
            string line3 = @" /      \/  \_/\  /     \/   \   /    \/\  /     \/";
            string line4 = @"/               \/            \_/        \/";

            int index = Math.Max(0, line1.Length - count + 1);

            return
                line0 + Environment.NewLine +
                line1.Substring(index) + Environment.NewLine +
                line2.Substring(index) + Environment.NewLine +
                line3.Substring(index) + Environment.NewLine +
                line4.Substring(index);
        }

        /// <summary>
        /// The last line(s) of an old log instance in the file if logging was abruptly terminated.
        /// <see cref="TimeStampKeyword"/> is replaced by a timestamp of the current moment.
        /// </summary>
        internal string AbruptEndingWithTimestamp
        {
            get
            {
                return AbruptEnding.Replace(_TimeStampKeyword, DateTime.Now.ToString(TimeStampFormat));
            }
        }
    }
}
