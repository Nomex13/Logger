﻿namespace Iodynis.Libraries.Logging
{
    /// <summary>
    /// Logger output to a database.
    /// </summary>
    public class LoggerOutputDatabase : ALoggerOutput
    {
        private string Url;
        /// <summary>
        /// Instantiate a logger to the specified database.
        /// </summary>
        /// <param name="name">The name of the logger.</param>
        /// <param name="url">The connection string for the database.</param>
        public LoggerOutputDatabase(string name, string url)
            : base(name)
        {
            Url = url;
        }
    }
}
