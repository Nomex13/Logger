﻿using System;

namespace Iodynis.Libraries.Logging
{
    [Serializable]
    public class LoggerLevelException : Exception
    {
        public LoggerLevelException() : base() { }
        public LoggerLevelException(string message) : base(message) { }
        public LoggerLevelException(string message, Exception innerException) : base(message, innerException) { }
    }
}
