﻿using System;
using System.Linq;

namespace Iodynis.Libraries.Logging
{
    internal class LoggerTimerManager
    {
        //private DethreadedStack<LoggerTimer> Stack = new DethreadedStack<LoggerTimer>();
        private DethreadedList<LoggerTimer> Timers = new DethreadedList<LoggerTimer>();
        private Pool<LoggerTimer> TimersPool;
        private Pool<LoggerTimerScope> ScopesPool;

        /// <summary>
        /// Timestamp format to use. The default is <see cref="LoggerTimeFormat.SHORT"/>.
        /// </summary>
        public LoggerTimeFormat TimerFormat { get; set; } = LoggerTimeFormat.SHORT;

        /// <summary>
        /// Template to replace with actual timestamp. The default is %time%.
        /// </summary>
        public string TimerTemplate { get; set; } = "%time%";

        internal LoggerTimerManager()
        {
            TimersPool = new Pool<LoggerTimer>(() => new LoggerTimer());
            ScopesPool = new Pool<LoggerTimerScope>(() => new LoggerTimerScope());
        }

        internal LoggerTimer Timer => Timers.Count == 0 ? null : Timers.Last();
        internal TimeSpan Time => Timers.Count == 0 ? TimeSpan.Zero : Timers.Last().Time;
        internal LoggerType GetLevel(int? cautionInMilliseonds = null, int? warningInMilliseonds = null, int? errorInMilliseonds = null)
        {
            if (Timers.Count == 0)
            {
                return LoggerType.TRACE;
            }

            return Timers.Last().GetLevel(cautionInMilliseonds, warningInMilliseonds, errorInMilliseonds);
        }

        internal IDisposable Scope(string start, string status = null, string finish = null, LoggerType level = LoggerType.TRACE, int? cautionInMilliseonds = null, int? warningInMilliseonds = null, int? errorInMilliseonds = null)
        {
            LoggerTimer timer = Start(start, status, finish, level, cautionInMilliseonds, warningInMilliseonds, errorInMilliseonds);

            LoggerTimerScope scope = ScopesPool.Allocate();
            scope.Timer = timer;
            return scope;
        }
        internal LoggerTimer Start(string text, string status = null, string finish = null, LoggerType level = LoggerType.TRACE, int? cautionInMilliseonds = null, int? warningInMilliseonds = null, int? errorInMilliseonds = null)
        {
            LoggerTimer timer = TimersPool.Allocate();
            timer.Set(text, status, finish, level, cautionInMilliseonds, warningInMilliseonds, errorInMilliseonds);
            //Stack.Push(timer);
            Timers.Add(timer);
            timer.Start();
            return timer;
        }

        internal void Stop()
        {
            if (Timers.Count == 0)
            {
                return;
            }

            // Stop the last timer
            LoggerTimer timer = Timers[Timers.Count - 1];
            Timers.RemoveAt(Timers.Count - 1);
            TimersPool.Deallocate(timer);
        }
        //internal void Stop(LoggerTimer timer)
        //{
        //    if (Timers.Count == 0)
        //    {
        //        return;
        //    }

        //    for (int index = Timers.Count - 1; index >= 0; index--)
        //    {
        //        if (Timers[index] == timer)
        //        {
        //            Timers.RemoveAt(index);
        //        }
        //    }

        //    TimersPool.Deallocate(timer);
        //}
    }
}
