﻿using System;

namespace Iodynis.Libraries.Logging
{
    internal class LoggerTimerScope : IDisposable
    {
        public LoggerTimer Timer;
        public LoggerTimerScope() { }
        public void Dispose()
        {
            Timer.Stop();
            Timer = null;
        }
    }
}
