﻿using System;
using System.Diagnostics;
using System.Text;

namespace Iodynis.Libraries.Logging
{
    internal class LoggerTimer
    {
        public string TextStart { get; private set; }
        public string TextIntermediate { get; private set; }
        public string TextFinish { get; private set; }

        public LoggerType Level { get; private set; }
        public int? CautionInMilliseonds { get; private set; }
        public int? WarningInMilliseonds { get; private set; }
        public int? ErrorInMilliseonds { get; private set; }

        private Stopwatch Stopwatch;

        public TimeSpan Time => Stopwatch.Elapsed;

        internal LoggerTimer()
        {
            Stopwatch = new Stopwatch();
        }
        internal LoggerType GetLevel(int? timeInMilliseondsTillCaution = null, int? timeInMilliseondsTillWarning = null, int? timeInMilliseondsTillError = null)
        {
            long milliseconds = Stopwatch.Elapsed.Ticks / 10000;

            if (timeInMilliseondsTillError.HasValue && timeInMilliseondsTillError.Value < milliseconds)
            {
                return LoggerType.ERROR;
            }
            else if (timeInMilliseondsTillWarning.HasValue && timeInMilliseondsTillWarning.Value < milliseconds)
            {
                return LoggerType.WARNING;
            }
            else if (timeInMilliseondsTillCaution.HasValue && timeInMilliseondsTillCaution.Value < milliseconds)
            {
                return LoggerType.CAUTION;
            }
            else
            {
                return Level;
            }
        }

        internal void Set(string textStart, string textIntermediate = null, string textFinish = null, LoggerType level = LoggerType.TRACE, int? cautionInMilliseonds = null, int? warningInMilliseonds = null, int? errorInMilliseonds = null)
        {
            TextStart = textStart;
            TextIntermediate = textIntermediate;
            TextFinish = textFinish;

            Level = level;
            CautionInMilliseonds = cautionInMilliseonds;
            WarningInMilliseonds = warningInMilliseonds;
            ErrorInMilliseonds = errorInMilliseonds;
        }
        internal void Reset()
        {
            TextStart = null;
            TextIntermediate = null;
            TextFinish = null;

            Level = LoggerType.TRACE;
            CautionInMilliseonds = null;
            WarningInMilliseonds = null;
            ErrorInMilliseonds = null;
        }

        internal void Start()
        {
            Stopwatch.Restart();
        }
        internal void Stop()
        {
            Stopwatch.Stop();
        }

        /// <summary>
        /// Get the text with imprinted time.
        /// </summary>
        /// <param name="text">The message.</param>
        /// <param name="timeTemplate">The time template to replace with time.</param>
        /// <param name="timeFormat">The time format.</param>
        public string GetTimedText(string text, string timeTemplate, LoggerTimeFormat timeFormat)
        {
            string textTime = GetTime(timeFormat);
            // If no text provided
            if (text == null || text.Length == 0)
            {
                text = textTime;
                return text;
            }
            // If %time% template is presented
            int index = text.IndexOf(timeTemplate);
            if (index >= 0)
            {
                text = text.Substring(0, index) + textTime + text.Substring(index + timeTemplate.Length);
                return text;
            }
            // If ends in a dot
            if (text.EndsWith("."))
            {
                text = $"{text.Substring(0, text.Length - 1)}, {textTime}.";
                return text;
            }
            // If ends with an unknwon symbol
            text = $"{text} {textTime}.";
            return text;
        }

        /// <summary>
        /// Get formatted time.
        /// </summary>
        /// <param name="format">The time format.</param>
        public string GetTime(LoggerTimeFormat format)
        {
            TimeSpan time = Stopwatch.Elapsed;
            if (time.Ticks == 0)
            {
                return "0";
            }

            // No formatting, just ticks.
            if (format == LoggerTimeFormat.NONE)
            {
                return time.Ticks.ToString();
            }

            // Short format, like: 1h 4m, 4m 5s, 8.12s, 9.777ms
            if (format == LoggerTimeFormat.SHORT)
            {
                StringBuilder stringBuilder = new StringBuilder();

                if (time.Days != 0)
                {
                    stringBuilder.Append($"{time.Days}d {time.Hours}h");
                }
                else if (time.Hours != 0)
                {
                    stringBuilder.Append($"{time.Hours}h {time.Minutes}m");
                }
                else if (time.Minutes != 0)
                {
                    stringBuilder.Append($"{time.Minutes}m {time.Seconds}s");
                }
                else if (time.Seconds != 0)
                {
                    stringBuilder.Append($"{time.Seconds}.{time.Milliseconds}s");
                }
                else if (time.Milliseconds != 0)
                {
                    if (Stopwatch.IsHighResolution)
                    {
                        int nanoseconds = (int)((time.Ticks / 10) % 1000);
                        stringBuilder.Append($"{time.Milliseconds}.{nanoseconds}ms");
                    }
                    else
                    {
                        stringBuilder.Append($"{time.Milliseconds}ms");
                    }
                }
                else
                {
                    int nanoseconds = (int)((time.Ticks / 10) % 1000);
                    stringBuilder.Append($"{nanoseconds}ns");
                }

                return stringBuilder.ToString();
            }

            // Long format, like: 1 hour and 4 minutes, 4 minutes and 5 seconds, 8 seconds and 12 milliseconds, 9 milliseconds and 777 nanoseconds
            if (format == LoggerTimeFormat.LONG)
            {
                StringBuilder stringBuilder = new StringBuilder();

                if (time.Days != 0)
                {
                    stringBuilder.Append($"{time.Days} day{(time.Days == 1 ? "" : "s")} and {time.Hours} hour{(time.Hours == 1 ? "" : "s")}");
                }
                else if (time.Hours != 0)
                {
                    stringBuilder.Append($"{time.Hours} hour{(time.Hours == 1 ? "" : "s")} and {time.Minutes} minute{(time.Minutes == 1 ? "" : "s")}");
                }
                else if (time.Minutes != 0)
                {
                    stringBuilder.Append($"{time.Minutes} minute{(time.Minutes == 1 ? "" : "s")} and {time.Seconds} second{(time.Seconds == 1 ? "" : "s")}");
                }
                else if (time.Seconds != 0)
                {
                    stringBuilder.Append($"{time.Seconds} second{(time.Seconds == 1 ? "" : "s")} and {time.Milliseconds} millisecond{(time.Milliseconds == 1 ? "" : "s")}");
                }
                else if (time.Milliseconds != 0)
                {
                    if (Stopwatch.IsHighResolution)
                    {
                        int nanoseconds = (int)((time.Ticks / 10) % 1000);
                        stringBuilder.Append($"{time.Milliseconds} millisecond{(time.Milliseconds == 1 ? "" : "s")} and {nanoseconds} nanosecond{(nanoseconds == 1 ? "" : "s")}");
                    }
                    else
                    {
                        stringBuilder.Append($"{time.Milliseconds} millisecond{(time.Milliseconds == 1 ? "" : "s")}");
                    }
                }
                else
                {
                    int nanoseconds = (int)((time.Ticks / 10) % 1000);
                    stringBuilder.Append($"{nanoseconds} nanosecond{(nanoseconds == 1 ? "" : "s")}");
                }

                return stringBuilder.ToString();
            }

            throw new Exception($"Time format {format} is not supported.");
        }
    }
}
