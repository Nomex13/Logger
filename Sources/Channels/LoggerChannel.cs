﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace Iodynis.Libraries.Logging
{
    /// <summary>
    /// Provides log functionalty.
    /// </summary>
    public class LoggerChannel
    {
        #region Properties

        private readonly Dictionary<string, ALoggerOutput> _Outputs = new Dictionary<string, ALoggerOutput>();

        /// <summary>
        /// Each thread has its own scope.
        /// One scope object handles all the nesting for its thread by storing a plain list of lines and a list of indexes of particular said lines where the nesting goes deeper.
        /// Once created, the scope is left for reuse by its thread even after nesting surfaces and it prints out its accumulated content.
        /// </summary>
        private readonly List<LoggerChannelScope> Scopes = new List<LoggerChannelScope>();

        /// <summary>
        /// The outputs this channel will log to.
        /// </summary>
        public IEnumerable<ALoggerOutput> Outputs => _Outputs.Values;

        /// <summary>
        /// Event triggers on every log operation.
        /// </summary>
        public event EventHandler<LoggerEventArguments> OnLog;

        /// <summary>
        /// Threshold severity level to log.
        /// </summary>
        public LoggerSeverity SeverityThreshold = LoggerSeverity.TRACE;

        /// <summary>
        /// Thread IDs to allow logs from.
        /// </summary>
        public readonly List<int> ThreadsAllowed = new List<int>();

        /// <summary>
        /// Thread IDs to deny logs from.
        /// </summary>
        public readonly List<int> ThreadsForbidden = new List<int>();

        /// <summary>
        /// Is logger channel already initialized.
        /// </summary>
        protected internal bool IsInitialized { get; private set; } = false;

        /// <summary>
        /// If should perform the logging.
        /// </summary>
        public bool IsSuppressed => _Suppressor.IsInScope;
        private LoggerSuppressor _Suppressor = new LoggerSuppressor();
        /// <summary>
        /// For use in using statements only.
        /// </summary>
        public LoggerSuppressor Suppress() => _Suppressor.ScopeStart();

        /// <summary>
        /// Logger manager that uses this logger channel.
        /// </summary>
        public LoggerManager Manager { get; private set; }

        /// <summary>
        /// Name is used for convenience only.
        /// </summary>
        public string Name { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Instantiate a logger channel with the specified name.
        /// </summary>
        /// <param name="name">The name of the logger output.</param>
        public LoggerChannel(string name)
        {
            Name = name;
        }
        /// <summary>
        /// Instanciate a logger prepopulated with the specified logger.
        /// </summary>
        public LoggerChannel(string name, params ALoggerOutput[] loggers)
            : this(name, loggers.AsEnumerable())
        {
            ;
        }
        /// <summary>
        /// Instanciate a logger prepopulated with the specified logger.
        /// </summary>
        public LoggerChannel(string name, IEnumerable<ALoggerOutput> loggers)
            : this(name)
        {
            AddOutputs(loggers);
        }

        #endregion

        /// <summary>
        /// Get a logger by its name.
        /// </summary>
        /// <param name="name">The name of a logger.</param>
        /// <returns>A logger.</returns>
        public ALoggerOutput this[string name]
        {
            get
            {
                return _Outputs[name];
            }
        }
        internal void Initialize(LoggerManager manager)
        {
            if (Manager != null)
            {
                throw new LoggerInitializationException($"Logger channel {this} is already used.");
            }

            Manager = manager;

            IsInitialized = true;
        }
        internal void Deinitialize()
        {
            Manager = null;

            IsInitialized = false;
        }
        /// <summary>
        /// Add multiple loggers.
        /// </summary>
        /// <param name="outputs">The loggers to add.</param>
        public void AddOutputs(IEnumerable<ALoggerOutput> outputs)
        {
            foreach (ALoggerOutput output in outputs)
            {
                AddOutput(output);
            }
        }
        /// <summary>
        /// Add a logger output.
        /// </summary>
        /// <param name="output">The logger output to add.</param>
        public void AddOutput(ALoggerOutput output)
        {
            if (output == null)
            {
                throw new ArgumentNullException(nameof(output));
            }
            if (_Outputs.ContainsKey(output.Name))
            {
                throw new Exception($"Logger output {output.Name} is already presented.");
            }

            output.Initialize(this);
            _Outputs.Add(output.Name, output);
        }
        /// <summary>
        /// Remove the specified logger output.
        /// </summary>
        /// <param name="output">The logger output to remove.</param>
        public void RemoveOutput(ALoggerOutput output)
        {
            if (output == null)
            {
                throw new ArgumentNullException(nameof(output));
            }
            if (!_Outputs.ContainsKey(output.Name))
            {
                throw new Exception($"Logger output {output.Name} is not presented.");
            }

            output.Deinitialize(this);
            _Outputs.Remove(output.Name);
        }

        #region Profiling

        /// <summary>
        /// Trace the time.
        /// </summary>
        /// <param name="level">The message level.</param>
        public LoggerChannel ProfileStart(LoggerType level = LoggerType.TRACE)
        {
            Manager.ProfileTimerManager.Start(null, null, null, level, null, null, null);
            return this;
        }
        /// <summary>
        /// Trace the time.
        /// </summary>
        /// <param name="text">The message.</param>
        /// <param name="level">The message level.</param>
        public LoggerChannel ProfileStart(string text, LoggerType level = LoggerType.TRACE)
        {
            Manager.ProfileTimerManager.Start(text, null, null, level, null, null, null);
            return Log(text, level);
        }
        /// <summary>
        /// Trace the time.
        /// </summary>
        /// <param name="start">The message at the scope start.</param>
        /// <param name="status">The message on the call to the <see cref="ProfileStatus"/>.</param>
        /// <param name="finish">The message at the scope end.</param>
        /// <param name="level">The default message level.</param>
        /// <param name="cautionInMilliseonds">If elapsed time exceeds this threshold, the message level will be raised to <see cref="LoggerType.CAUTION"/>.</param>
        /// <param name="warningInMilliseonds">If elapsed time exceeds this threshold, the message level will be raised to <see cref="LoggerType.WARNING"/>.</param>
        /// <param name="errorInMilliseonds">If elapsed time exceeds this threshold, the message level will be raised to <see cref="LoggerType.ERROR"/>.</param>
        public IDisposable ProfileScope(string start, string status, string finish, LoggerType level, int? cautionInMilliseonds = null, int? warningInMilliseonds = null, int? errorInMilliseonds = null)
        {
            return Manager.ProfileTimerManager.Scope(start, status, finish, level, cautionInMilliseonds, warningInMilliseonds, errorInMilliseonds);
        }
        /// <summary>
        /// Trace the time.
        /// </summary>
        /// <param name="start">The message at the scope start.</param>
        /// <param name="status">The message on the call to the <see cref="ProfileStatus"/>.</param>
        /// <param name="finish">The message at the scope end.</param>
        /// <param name="cautionInMilliseonds">If elapsed time exceeds this threshold, the message level will be raised to <see cref="LoggerType.CAUTION"/>.</param>
        /// <param name="warningInMilliseonds">If elapsed time exceeds this threshold, the message level will be raised to <see cref="LoggerType.WARNING"/>.</param>
        /// <param name="errorInMilliseonds">If elapsed time exceeds this threshold, the message level will be raised to <see cref="LoggerType.ERROR"/>.</param>
        public IDisposable ProfileScope(string start, string status, string finish, int? cautionInMilliseonds = null, int? warningInMilliseonds = null, int? errorInMilliseonds = null)
        {
            return Manager.ProfileTimerManager.Scope(start, status, finish, LoggerType.TRACE, cautionInMilliseonds, warningInMilliseonds, errorInMilliseonds);
        }
        /// <summary>
        /// Print the state of the timer at the top of the timers stack.
        /// </summary>
        public LoggerChannel ProfileStatus(string text = null, int? cautionInMilliseonds = null, int? warningInMilliseonds = null, int? errorInMilliseonds = null)
        {
            LoggerType level = Manager.ProfileTimerManager.GetLevel(cautionInMilliseonds, warningInMilliseonds, errorInMilliseonds);
            string textTimed = Manager.ProfileTimerManager.Timer.GetTimedText(text, Manager.ProfileTimerTemplate, Manager.ProfileTimerFormat);
            return Log(textTimed, level);
        }
        /// <summary>
        /// Print the state of the timer at the top of the timers stack.
        /// </summary>
        public LoggerChannel ProfileStop(string text = null, int? cautionInMilliseonds = null, int? warningInMilliseonds = null, int? errorInMilliseonds = null)
        {
            ProfileStatus(text, cautionInMilliseonds, warningInMilliseonds, errorInMilliseonds);
            Manager.ProfileTimerManager.Stop();
            return this;
        }

        #endregion

        #region Conditional

        /// <summary>
        /// Continue ony if the condition is true.
        /// </summary>
        /// <param name="condition">Condition to check.</param>
        /// <returns>Condition wrapper for this channel.</returns>
        public LoggerChannelConditional If(bool condition)
        {
            return new LoggerChannelConditional(this, condition);
        }

        #endregion

        #region Scope

        /// <summary>
        /// Create a scope wrapper that will close upon disposal.
        /// </summary>
        /// <returns>Scope wrapper for this channel.</returns>
        public LoggerChannelScopeDisposable Scope()
        {
            return new LoggerChannelScopeDisposable(this);
        }
        private LoggerChannelScope CreateScope()
        {
            LoggerChannelScope scope = new LoggerChannelScope(this);
            lock (Scopes)
            {
                for (int ScopeIndex = 0; ScopeIndex < Scopes.Count; ScopeIndex++)
                {
                    if (Scopes[ScopeIndex] == null)
                    {
                        Scopes[ScopeIndex] = scope;
                        return scope;
                    }
                }

                Scopes.Add(scope);
                return scope;
            }
        }
        /// <summary>
        /// Search for the active topmost scope for the current thread.
        /// </summary>
        /// <returns></returns>
        private int GetScopeIndex()
        {
            if (Scopes.Count == 0)
            {
                return -1;
            }

            int threadId = Thread.CurrentThread.ManagedThreadId;
            for (int ScopeIndex = 0; ScopeIndex < Scopes.Count; ScopeIndex++)
            {
                if (Scopes[ScopeIndex] != null && Scopes[ScopeIndex].ThreadId == threadId)
                {
                    return ScopeIndex;
                }
            }

            return -1;
        }
        /// <summary>
        /// Search for the active topmost scope for the current thread.
        /// </summary>
        /// <returns></returns>
        private LoggerChannelScope GetScope()
        {
            if (Scopes.Count == 0)
            {
                return null;
            }

            int threadId = Thread.CurrentThread.ManagedThreadId;
            for (int ScopeIndex = 0; ScopeIndex < Scopes.Count; ScopeIndex++)
            {
                if (Scopes[ScopeIndex] != null && Scopes[ScopeIndex].ThreadId == threadId)
                {
                    return Scopes[ScopeIndex];
                }
            }

            return null;
        }
        /// <summary>
        /// Search for the active topmost scope for the current thread, create a new one if not found.
        /// </summary>
        /// <returns></returns>
        private LoggerChannelScope GetOrCreateScope()
        {
            return GetScope() ?? CreateScope();
        }
        /// <summary>
        /// Start a new scope, increasing the indentation.
        /// </summary>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel ScopeStart()
        {
            LoggerChannelScope scope = GetOrCreateScope();

            scope.ScopeStart();

            return this;
        }
        /// <summary>
        /// Finish the current scope, decreasing the indentation.
        /// </summary>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        /// <exception cref="Exception">Throws an exception if called more times than the <see cref="ScopeStart()"/> method (the current scope is already the outmost one).</exception>
        public LoggerChannel ScopeEnd()
        {
            LoggerChannelScope scope = GetScope();

            // Not in a scope
            if (scope == null)
            {
                return this;
            }

            scope.ScopeEnd();

            // Going out of th last scope
            if (scope.Depth == 0)
            {
                scope.Log();
            }
            
            return this;
        }
        /// <summary>
        /// Finish all the scopes, decreasing the indentation to none.
        /// </summary>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel ScopeEndAll()
        {
            LoggerChannelScope scope = GetScope();

            // Not in a scope
            if (scope == null)
            {
                return this;
            }

            scope.ScopeEndAll();
            scope.Log();
            
            return this;
        }

        #endregion

        #region Append

        /// <summary>
        /// <inheritdoc cref="ALoggerOutput.Append(string, string)"/>
        /// </summary>
        public LoggerChannel Append(string text, string delimiter = null)
        {
            if (IsSuppressed)
            {
                return this;
            }

            foreach (ALoggerOutput logger in _Outputs.Values)
            {
                logger.Append(text, delimiter);
            }

            return this;
        }
        /// <summary>
        /// <inheritdoc cref="ALoggerOutput.Append(string, string)"/>
        /// </summary>
        public LoggerChannel Append<T>(T @object, string delimiter = null)
        {
            if (IsSuppressed)
            {
                return this;
            }

            Append(Manager.Printer.Print(@object), delimiter);

            return this;
        }

        #endregion

        #region Trace

        /// <summary>
        /// Auto-trace the current position in the code.
        /// </summary>
        /// <param name="methodName">Method name.</param>
        /// <param name="sourcePath">Source file path.</param>
        /// <param name="sourceLine">Line number in the source file.</param>
        [Conditional("DEBUG")]
        public void Trace(
            [CallerMemberName] string methodName = "",
            [CallerFilePath]   string sourcePath = "",
            [CallerLineNumber] int sourceLine = 0)
        {
            Log($"Method {methodName} in {sourcePath} at {sourceLine}.", LoggerType.TRACE);
        }
        /// <summary>
        /// <inheritdoc cref="Trace(string, Exception)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="Trace(string, Exception)" path="/param[1]"/></param>
        [Conditional("DEBUG")]
        public void Trace(string text = "")
        {
            Log(text, LoggerType.TRACE);
        }
        /// <summary>
        /// <inheritdoc cref="Trace(string, Exception)"/>
        /// </summary>
        /// <param name="exception"><inheritdoc cref="Trace(string, Exception)" path="/param[2]"/></param>
        [Conditional("DEBUG")]
        public void Trace(Exception exception)
        {
            Log(GetExceptionFullMessage(exception), LoggerType.TRACE);
        }
        /// <summary>
        /// Log a trace info.
        /// Supposed to say "i'm here".
        /// </summary>
        /// <param name="text">Information to log.</param>
        /// <param name="exception">Exception to log.</param>
        [Conditional("DEBUG")]
        public void Trace(string text, Exception exception)
        {
            Log(GetExceptionFullMessage(exception, text), LoggerType.TRACE);
        }

        #endregion

        #region Debug

        /// <summary>
        /// <inheritdoc cref="Debug(string, Exception)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="Debug(string, Exception)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Debug(string text = "")
        {
            return Log(text, LoggerType.DEBUG);
        }
        /// <summary>
        /// <inheritdoc cref="Debug(string, Exception)"/>
        /// </summary>
        /// <param name="exception"><inheritdoc cref="Debug(string, Exception)" path="/param[2]"/></param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Debug(Exception exception)
        {
            return Log(GetExceptionFullMessage(exception), LoggerType.DEBUG);
        }
        /// <summary>
        /// Log a debug info.
        /// Supposed to say "state is ***".
        /// </summary>
        /// <param name="text">Information to log.</param>
        /// <param name="exception">Exception to log.</param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Debug(string text, Exception exception)
        {
            return Log(GetExceptionFullMessage(exception, text), LoggerType.DEBUG);
        }

        #endregion

        #region Information

        /// <summary>
        /// <inheritdoc cref="Information(string, Exception)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="Information(string, Exception)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Information(string text = "")
        {
            return Log(text, LoggerType.INFORMATION);
        }
        /// <summary>
        /// <inheritdoc cref="Information(string, Exception)"/>
        /// </summary>
        /// <param name="exception"><inheritdoc cref="Information(string, Exception)" path="/param[2]"/></param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Information(Exception exception)
        {
            return Log(GetExceptionFullMessage(exception), LoggerType.INFORMATION);
        }
        /// <summary>
        /// Log an info.
        /// </summary>
        /// <param name="text">Information to log.</param>
        /// <param name="exception">Exception to log.</param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Information(string text, Exception exception)
        {
            return Log(GetExceptionFullMessage(exception, text), LoggerType.INFORMATION);
        }

        #endregion

        #region Notice

        /// <summary>
        /// <inheritdoc cref="Notice(string, Exception)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="Notice(string, Exception)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Notice(string text = "")
        {
            return Log(text, LoggerType.NOTICE);
        }
        /// <summary>
        /// <inheritdoc cref="Notice(string, Exception)"/>
        /// </summary>
        /// <param name="exception"><inheritdoc cref="Notice(string, Exception)" path="/param[2]"/></param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Notice(Exception exception)
        {
            return Log(GetExceptionFullMessage(exception), LoggerType.NOTICE);
        }
        /// <summary>
        /// Log a notice.
        /// </summary>
        /// <param name="text">Notice description.</param>
        /// <param name="exception">Exception that caused the notice.</param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Notice(string text, Exception exception)
        {
            return Log(GetExceptionFullMessage(exception, text), LoggerType.NOTICE);
        }

        #endregion

        #region Caution

        /// <summary>
        /// <inheritdoc cref="Caution(string, Exception)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="Caution(string, Exception)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Caution(string text = "")
        {
            return Log(text, LoggerType.CAUTION);
        }
        /// <summary>
        /// <inheritdoc cref="Caution(string, Exception)"/>
        /// </summary>
        /// <param name="exception"><inheritdoc cref="Caution(string, Exception)" path="/param[2]"/></param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Caution(Exception exception)
        {
            return Log(GetExceptionFullMessage(exception), LoggerType.CAUTION);
        }
        /// <summary>
        /// Log a caution.
        /// </summary>
        /// <param name="text">Caution description.</param>
        /// <param name="exception">Exception that caused the caution.</param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Caution(string text, Exception exception)
        {
            return Log(GetExceptionFullMessage(exception, text), LoggerType.CAUTION);
        }

        #endregion

        #region Warning

        /// <summary>
        /// <inheritdoc cref="Warning(string, Exception)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="Warning(string, Exception)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Warning(string text = "")
        {
            return Log(text, LoggerType.WARNING);
        }
        /// <summary>
        /// <inheritdoc cref="Warning(string, Exception)"/>
        /// </summary>
        /// <param name="exception"><inheritdoc cref="Warning(string, Exception)" path="/param[2]"/></param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Warning(Exception exception)
        {
            return Log(GetExceptionFullMessage(exception), LoggerType.WARNING);
        }
        /// <summary>
        /// Log a warning.
        /// </summary>
        /// <param name="text">Warning description.</param>
        /// <param name="exception">Exception that caused the warning.</param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Warning(string text, Exception exception)
        {
            return Log(GetExceptionFullMessage(exception, text), LoggerType.WARNING);
        }

        #endregion

        #region Error

        /// <summary>
        /// <inheritdoc cref="Error(string, Exception)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="Error(string, Exception)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Error(string text = "")
        {
            return Log(text, LoggerType.ERROR);
        }
        /// <summary>
        /// <inheritdoc cref="Error(string, Exception)"/>
        /// </summary>
        /// <param name="exception"><inheritdoc cref="Error(string, Exception)" path="/param[2]"/></param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Error(Exception exception)
        {
            return Log(GetExceptionFullMessage(exception), LoggerType.ERROR);
        }
        /// <summary>
        /// Log a recoverable error.
        /// </summary>
        /// <param name="text">Error description.</param>
        /// <param name="exception">Exception that caused the error.</param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Error(string text, Exception exception)
        {
            return Log(GetExceptionFullMessage(exception, text), LoggerType.ERROR);
        }

        #endregion

        #region Fatality

        /// <summary>
        /// <inheritdoc cref="Fatality(string, Exception)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="Fatality(string, Exception)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Fatality(string text = "")
        {
            return Log(text, LoggerType.FATALITY);
        }
        /// <summary>
        /// <inheritdoc cref="Fatality(string, Exception)"/>
        /// </summary>
        /// <param name="exception"><inheritdoc cref="Fatality(string, Exception)" path="/param[2]"/></param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Fatality(Exception exception)
        {
            return Log(GetExceptionFullMessage(exception), LoggerType.FATALITY);
        }
        /// <summary>
        /// Log a fatal unrecoverable error.
        /// </summary>
        /// <param name="text">Crash description.</param>
        /// <param name="exception">Exception that caused the crash.</param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Fatality(string text, Exception exception)
        {
            return Log(GetExceptionFullMessage(exception, text), LoggerType.FATALITY);
        }

        #endregion

        #region Success

        /// <summary>
        /// Log a small success.
        /// </summary>
        /// <param name="text">Description.</param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Success(string text = "")
        {
            return Log(text, LoggerType.SUCCESS);
        }

        #endregion

        #region Win

        /// <summary>
        /// Log a huge success.
        /// </summary>
        /// <param name="text">Description.</param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Win(string text = "")
        {
            return Log(text, LoggerType.WIN);
        }

        #endregion

        #region Log

        /// <summary>
        /// Log an empty line.
        /// </summary>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel NewLine()
        {
            return Log("", LoggerType.NONE);
        }

        /// <summary>
        /// Log the text of the specified type.
        /// </summary>
        /// <param name="type">Message type.</param>
        /// <param name="object">Object.</param>
        /// <returns><inheritdoc cref="Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannel Object<T>(T @object, LoggerType type = LoggerType.NONE)
        {
            if (IsSuppressed)
            {
                return this;
            }

            return Log(Manager.Printer.Print(@object), type);
        }

        /// <summary>
        /// Log the text of the specified type.
        /// </summary>
        /// <param name="text">Message text.</param>
        /// <param name="type">Message type.</param>
        /// <returns>The chainable logger channel.</returns>
        public LoggerChannel Log(string text, LoggerType type = LoggerType.NONE)
        {
            // Redirect to active scope
            if (Scopes.Count > 0)
            {
                LoggerChannelScope scope = GetScope();
                if (scope != null && scope.Depth > 0)
                {
                    scope.Log(type, text);
                    return this;
                }
            }

            if (IsSuppressed)
            {
                return this;
            }
            if (ThreadsAllowed.Count > 0 && !ThreadsAllowed.Contains(Thread.CurrentThread.ManagedThreadId))
            {
                return this;
            }
            if (ThreadsForbidden.Count > 0 && ThreadsForbidden.Contains(Thread.CurrentThread.ManagedThreadId))
            {
                return this;
            }
            if (type != LoggerType.NONE && SeverityThreshold != LoggerSeverity.NONE && type.ToSeverity() < SeverityThreshold)
            {
                return this;
            }

            DateTime time = DateTime.Now;
            foreach (ALoggerOutput logger in _Outputs.Values)
            {
                logger.Log(Name, text, type, time, 0);
            }
            OnLog?.Invoke(this, new LoggerEventArguments(text, type, time));

            return this;
        }
        /// <summary>
        /// Log the line accumulated by the scope.
        /// </summary>
        /// <param name="line"></param>
        internal void Log(LoggerChannelLine line)
        {
            if (IsSuppressed)
            {
                return;
            }
            if (ThreadsAllowed.Count > 0 && !ThreadsAllowed.Contains(Thread.CurrentThread.ManagedThreadId))
            {
                return;
            }
            if (ThreadsForbidden.Count > 0 && ThreadsForbidden.Contains(Thread.CurrentThread.ManagedThreadId))
            {
                return;
            }
            if (line.Type != LoggerType.NONE && SeverityThreshold != LoggerSeverity.NONE && line.Type.ToSeverity() < SeverityThreshold)
            {
                return;
            }

            foreach (ALoggerOutput logger in _Outputs.Values)
            {
                logger.Log(Name, line.Text, line.Type, line.Timestamp, line.Depth);
            }
        }
        /// <summary>
        /// Log the lines accumulated by the scope.
        /// </summary>
        /// <param name="lines"></param>
        internal void Log(List<LoggerChannelLine> lines)
        {
            if (IsSuppressed)
            {
                return;
            }
            if (ThreadsAllowed.Count > 0 && !ThreadsAllowed.Contains(Thread.CurrentThread.ManagedThreadId))
            {
                return;
            }
            if (ThreadsForbidden.Count > 0 && ThreadsForbidden.Contains(Thread.CurrentThread.ManagedThreadId))
            {
                return;
            }

            Dictionary<string, ALoggerOutput>.ValueCollection outputs = _Outputs.Values;
            for (int lineIndex = 0; lineIndex < lines.Count; lineIndex++)
            {
                LoggerChannelLine line = lines[lineIndex];
                foreach (ALoggerOutput logger in outputs)
                {
                    if (line.Type != LoggerType.NONE && SeverityThreshold != LoggerSeverity.NONE && line.Type.ToSeverity() < SeverityThreshold)
                    {
                        continue;
                    }

                    logger.Log(Name, line.Text, line.Type, line.Timestamp, line.Depth);
                }
            }
        }

        #endregion

        #region Time

        ///// <summary>
        ///// Log the text of the specified type.
        ///// </summary>
        ///// <param name="text">Description.</param>
        ///// <param name="type">Message type.</param>
        //private void Timeit(string text, LoggerType type)
        //{
        //    DateTime time = DateTime.Now;

        //    LoggerSeverity level = LoggerTypeToLevel(type);
        //    if (Severity == LoggerSeverity.NONE || level > Severity)
        //    {
        //        return;
        //    }

        //    foreach (ALogger logger in Loggers.Values)
        //    {
        //        if (level > logger.Level)
        //        {
        //            continue;
        //        }
        //        logger.Log(text, type, time);
        //    }

        //    OnLog?.Invoke(this, new LoggerEventArguments(text, type, time));
        //}

        #endregion

        #region Util
        /// <summary>
        /// Get the full message for the exception, including all the inner exceptions messages.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <param name="text">Custom additional text to print before the exception.</param>
        /// <returns>The full message for the exception.</returns>
        public static string GetExceptionFullMessage(Exception exception, string text = null)
        {
            if (exception == null)
            {
                return text;
            }

            // Collect all the messages
            var messages = new List<string>();
            Exception exceptionInner = exception;
            while (true)
            {
                messages.Add($"{exceptionInner.Message} ({exceptionInner.GetType()})");
                if (exceptionInner.InnerException == null)
                {
                    break;
                }
                exceptionInner = exceptionInner.InnerException;
            }

            // Compose the full message
            StringBuilder stringBuilder = new StringBuilder();
            if (text != null)
            {
                stringBuilder.Append($"{text}\r\n");
            }
            else
            {
                stringBuilder.Append($"{exception.Message}\r\n");
            }
            for (var i = messages.Count - 1; i >= 0; i--)
            {
                stringBuilder.Append($"\r\n{messages.Count - i}. {messages[i]}");
            }

            // Append the stack trace
            stringBuilder.Append($"\r\n\r\nStack trace:\r\n{exception.StackTrace}\r\n");

            return stringBuilder.ToString();
        }
        #endregion
    }
}
