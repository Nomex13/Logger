﻿using System;

namespace Iodynis.Libraries.Logging
{
    public class LoggerChannelScopeDisposable : IDisposable
    {
        private readonly LoggerChannel Channel;

        internal LoggerChannelScopeDisposable(LoggerChannel channel)
        {
            Channel = channel;
            Channel.ScopeStart();
        }
        public void Dispose()
        {
            Channel.ScopeEnd();
        }
    }
}
