﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Iodynis.Libraries.Logging
{
    internal class LoggerChannelScope
    {
        /// <summary>
        /// The channel this scope is associated with.
        /// </summary>
        public readonly LoggerChannel Channel;
        /// <summary>
        /// The thread ID of the thread that this scope is associated with.
        /// </summary>
        public readonly int ThreadId = -1;
        /// <summary>
        /// The lines collected by scoping.
        /// All of them will be printed once the the outermost scope closes (the <see cref="Depth"></see> reaches zero).
        /// </summary>
        private readonly List<LoggerChannelLine> Lines = new List<LoggerChannelLine>();
        /// <summary>
        /// The indexes of lines where inner scopes start and the nesting goes depper.
        /// </summary>
        private readonly List<int> Scopes = new List<int>();
        /// <summary>
        /// The index of the current line in the <see cref="Lines"></see> list.
        /// This is used to append to already logged lines, like append smth to the scope opening line.
        /// </summary>
        private int RecordIndex = 0;

        /// <summary>
        /// The depth of the current scope.
        /// </summary>
        public int Depth => Scopes.Count;

        public LoggerChannelScope(LoggerChannel channel)
        {
            Channel = channel;
            ThreadId = Thread.CurrentThread.ManagedThreadId;
        }

        public LoggerChannelScope At(int recordIndex)
        {
            RecordIndex =
                  recordIndex < -Lines.Count ? 0                           // Negative out of bounds
                : recordIndex < 0              ? Lines.Count + recordIndex // Negative
                : recordIndex <= Lines.Count ? recordIndex                 // Positive
                : Lines.Count;                                             // Positive out of bounds

            return this;
        }
        public LoggerChannelScope AtStart()
        {
            RecordIndex = 0;

            return this;
        }
        public LoggerChannelScope AtEnd()
        {
            RecordIndex = Lines.Count;

            return this;
        }
        public LoggerChannelScope AtEnd(int recordIndexFromEnd)
        {
            return At(Lines.Count - recordIndexFromEnd);
        }
        /// <summary>
        /// Start a new inner scope and increase the depth.
        /// </summary>
        public void ScopeStart()
        {
            Scopes.Add(Lines.Count);
        }
        /// <summary>
        /// End the innermost scope and decease the depth.
        /// </summary>
        public void ScopeEnd()
        {
            if (Scopes.Count == 0)
            {
                return;
            }

            Scopes.RemoveAt(Scopes.Count - 1);
        }
        /// <summary>
        /// End all the scopes and decrease the depth to zero.
        /// </summary>
        public void ScopeEndAll()
        {
            Scopes.Clear();
        }
        public LoggerChannel Unscope()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Debug(string)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="LoggerChannel.Debug(string)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Debug(string)" path="/returns"/></returns>
        public LoggerChannelScope Debug(string text = "")
        {
            Lines.Insert(RecordIndex++, new LoggerChannelLine(text, LoggerType.DEBUG, Depth));

            return this;
        }
        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Debug(Exception)"/>
        /// </summary>
        /// <param name="exception"><inheritdoc cref="LoggerChannel.Debug(Exception)" path="/param[1]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Debug(Exception)" path="/returns"/></returns>
        public LoggerChannelScope Debug(Exception exception)
        {
            Lines.Insert(RecordIndex++, new LoggerChannelLine(LoggerChannel.GetExceptionFullMessage(exception), LoggerType.DEBUG, Depth));

            return this;
        }
        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Debug(string, Exception)"/>
        /// </summary>
        /// <param name="text"><inheritdoc cref="LoggerChannel.Debug(string, Exception)" path="/param[1]"/></param>
        /// <param name="exception"><inheritdoc cref="LoggerChannel.Debug(string, Exception)" path="/param[2]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Debug(string, Exception)" path="/returns"/></returns>
        public LoggerChannelScope Debug(string text, Exception exception)
        {
            Lines.Insert(RecordIndex++, new LoggerChannelLine(text + "\r\n" + LoggerChannel.GetExceptionFullMessage(exception), LoggerType.DEBUG, Depth));

            return this;
        }
        /// <summary>
        /// <inheritdoc cref="LoggerChannel.Log(string, LoggerType)"/>
        /// </summary>
        /// <param name="type"><inheritdoc cref="LoggerChannel.Log(string, LoggerType)" path="/param[1]"/></param>
        /// <param name="text"><inheritdoc cref="LoggerChannel.Log(string, LoggerType)" path="/param[2]"/></param>
        /// <returns><inheritdoc cref="LoggerChannel.Log(string, LoggerType)" path="/returns"/></returns>
        public LoggerChannelScope Log(LoggerType type, string text)
        {
            Lines.Insert(RecordIndex++, new LoggerChannelLine(text, type, Depth));

            return this;
        }
        /// <summary>
        /// Log all the collected lines and reset.
        /// </summary>
        public void Log()
        {
            Channel.Log(Lines);
            Reset();
        }
        /// <summary>
        /// Clear all the collected lines.
        /// </summary>
        public void Reset()
        {
            Lines.Clear();
            RecordIndex = 0;
        }
    }
}
