﻿using System;
using System.Threading;

namespace Iodynis.Libraries.Logging
{
    public struct LoggerChannelLine
    {
        public string Text;
        public LoggerType Type;
        public int Depth;
        public DateTime Timestamp;
        public int ThreadId;

        public LoggerChannelLine(string text = null, LoggerType type = LoggerType.NONE, int depth = 0)
        {
            Text = text;
            Type = type;
            Depth = depth;
            Timestamp = DateTime.Now;
            ThreadId = Thread.CurrentThread.ManagedThreadId;
        }
    }
}
