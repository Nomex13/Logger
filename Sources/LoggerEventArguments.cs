﻿using System;

namespace Iodynis.Libraries.Logging
{
    /// <summary>
    /// Logged event information.
    /// </summary>
    public struct LoggerEventArguments
    {
        /// <summary>
        /// The message of the logged event.
        /// </summary>
        public readonly string Text;
        /// <summary>
        /// The type of the logged event.
        /// </summary>
        public readonly LoggerType Type;
        /// <summary>
        /// The time of the logged event.
        /// </summary>
        public readonly DateTime Timestamp;
        /// <summary>
        /// Logged event information.
        /// </summary>
        /// <param name="text">The message of the logged event.</param>
        /// <param name="type">The type of the logged event.</param>
        /// <param name="timestamp">The time of the logged event.</param>
        public LoggerEventArguments(string text, LoggerType type, DateTime timestamp)
        {
            Text = text;
            Type = type;
            Timestamp = timestamp;
        }
    }
}
