﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Logging
{
    /// <summary>
    /// Provides log functionalty.
    /// </summary>
    public class LoggerManager
    {
        private readonly Dictionary<string, LoggerChannel> _Channels = new Dictionary<string, LoggerChannel>();
        public IEnumerable<LoggerChannel> Channels => _Channels.Values;
        public readonly LoggerPrinter Printer = new LoggerPrinter();

        internal readonly LoggerTimerManager ProfileTimerManager;
        /// <summary>
        /// <inheritdoc cref="LoggerTimerManager.TimerFormat"/>
        /// </summary>
        public LoggerTimeFormat ProfileTimerFormat
        {
            get => ProfileTimerManager.TimerFormat;
            set => ProfileTimerManager.TimerFormat = value;
        }

        /// <summary>
        /// <inheritdoc cref="LoggerTimerManager.TimerTemplate"/>
        /// </summary>
        public string ProfileTimerTemplate
        {
            get => ProfileTimerManager.TimerTemplate;
            set => ProfileTimerManager.TimerTemplate = value;
        }

        public LoggerManager()
        {
            ProfileTimerManager = new LoggerTimerManager();
        }
        /// <summary>
        /// Get a logger channel by its name.
        /// </summary>
        /// <param name="name">The name of a logger.</param>
        /// <returns>A logger.</returns>
        public LoggerChannel this[string name]
        {
            get
            {
                return _Channels[name];
            }
        }
        /// <summary>
        /// Add multiple logger channels.
        /// </summary>
        /// <param name="channels">The loggers to add.</param>
        public void AddChannels(params LoggerChannel[] channels)
        {
            AddChannels(channels.AsEnumerable());
        }
        /// <summary>
        /// Add multiple logger channels.
        /// </summary>
        /// <param name="channels">The loggers to add.</param>
        public void AddChannels(IEnumerable<LoggerChannel> channels)
        {
            foreach (LoggerChannel channel in channels)
            {
                AddChannel(channel);
            }
        }
        /// <summary>
        /// Add a logger channel.
        /// </summary>
        /// <param name="channel">The logger channel to add.</param>
        public void AddChannel(LoggerChannel channel)
        {
            if (channel == null)
            {
                throw new ArgumentNullException(nameof(channel));
            }
            if (_Channels.ContainsKey(channel.Name))
            {
                throw new Exception($"Logger channel {channel.Name} is already presented.");
            }

            channel.Initialize(this);
            _Channels.Add(channel.Name, channel);
        }
        /// <summary>
        /// Remove the specified logger channel.
        /// </summary>
        /// <param name="channel">The logger channel to remove.</param>
        public void RemoveChannel(LoggerChannel channel)
        {
            if (channel == null)
            {
                throw new ArgumentNullException(nameof(channel));
            }
            if (!_Channels.ContainsKey(channel.Name))
            {
                throw new Exception($"Logger channel {channel.Name} is not presented.");
            }

            channel.Deinitialize();
            _Channels.Remove(channel.Name);
        }
    }
}
