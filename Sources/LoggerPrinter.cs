﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Logging
{
    public class LoggerPrinter
    {
        private LoggerPrinter PrinterFallback;
        private Dictionary<Type, Func<object, string>> _Printers = new Dictionary<Type, Func<object, string>>();

        public LoggerPrinter()
        {
            ;
        }

        public void SetFallback(LoggerPrinter printer)
        {
            PrinterFallback = printer;
        }
        public void Set<T>(Func<object, string> printer)
        {
            _Printers[typeof(T)] = printer;
        }
        public void Unset<T>()
        {
            _Printers.Remove(typeof(T));
        }

        public string Print<T>(T value)
        {
            return Print(typeof(T), value);
        }

        public string Print(object value)
        {
            return Print(value.GetType(), value);
        }

        private string Print(Type type, object value)
        {
            if (TryPrint(type, value, out var text))
            {
                return text;
            }

            return value.ToString();
        }

        public bool TryPrint<T>(T value, out string text)
        {
            return TryPrint(typeof(T), value, out text);
        }

        public bool TryPrint(object value, out string text)
        {
            return TryPrint(value.GetType(), value, out text);
        }

        private bool TryPrint(Type type, object value, out string text)
        {
            if (_Printers.TryGetValue(type, out var printer))
            {
                text = printer(value);
                return true;
            }

            if (PrinterFallback != null)
            {
                return PrinterFallback.TryPrint(type, value, out text);
            }

            text = value.ToString();
            return false;
        }
    }
}
