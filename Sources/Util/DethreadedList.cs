﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Iodynis.Libraries.Logging
{
    internal class DethreadedList<T> : IList<T>
    {
        private static Dictionary<int, List<T>> ThreadIdToStack = new Dictionary<int, List<T>>();

        public int Count => List.Count;

        public bool IsReadOnly => false;

        public T this[int index] { get => List[index]; set => List[index] = value; }

        internal DethreadedList()
        {
            ;
        }

        private List<T> List
        {
            get
            {
                if (!ThreadIdToStack.TryGetValue(Environment.CurrentManagedThreadId, out List<T> list))
                {
                    list = new List<T>();
                    ThreadIdToStack.Add(Environment.CurrentManagedThreadId, list);
                }
                return list;
            }
        }

        public int IndexOf(T item)
        {
            return List.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            List.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            List.RemoveAt(index);
        }

        public void Add(T item)
        {
            List.Add(item);
        }

        public void Clear()
        {
            List.Clear();
        }

        public bool Contains(T item)
        {
            return List.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            List.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            return List.Remove(item);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return List.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return List.GetEnumerator();
        }
    }
}
